# gEDA

Basic symbols for drawing diagrams with gEDA.

# How to use

Usually, on Debian based distros, the symbols are located at "/usr/share/gEDA/symbols" directory. To use these symbols, clone them all in this directory directly, or if you prefer, differentiating them in the "local" folder or any other within that path.